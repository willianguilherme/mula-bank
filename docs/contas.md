# Contas

## Conta corrente
- Dinheiro rende mensalmente
- Transferências ilimitadas
- 2 saques / mês
- Depósitos ilimitados
- Pagamentos ilimitados
- Limite progressivo, a cada 1 semana adiciona R$ 200,00 com máximo em o dobro da renda
- Juros do limite em 10%
- 1 cartão de crédito
- 1 cartão de débito

Pagando taxa mensal de R$ 10,00
- 5 saques / mês
- Cartão virtual

Pagando taxa mensal de R$ 20,00
- Saques ilimitados
- Cartão virtual
- Cartão por transação

## Conta poupança
- Dinheiro rende mensalmente
- 3 transferências para o mesmo banco / mês
- 2 transferências para outros bancos / mês
- 2 saques / mês
- Depósitos ilimitados
- Pagamentos ilimitados
- Não tem limite
- 1 cartão de débito

## Conta salário
- Receber dinheiro e transferir para outra conta
